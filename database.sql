-- PostgreSQL Script

-- Set schema
CREATE SCHEMA IF NOT EXISTS projetoava;

-- Set the search path to the schema
SET search_path TO projetoava;

-- -----------------------------------------------------
-- Table projetoava.user_types
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS user_types (
  id SERIAL PRIMARY KEY,
  type VARCHAR(10) NOT NULL
);

-- -----------------------------------------------------
-- Table projetoava.users
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS users (
  id SERIAL PRIMARY KEY,
  name VARCHAR(200) NOT NULL,
  login VARCHAR(200) NOT NULL UNIQUE,
  password VARCHAR(200) NOT NULL,
  user_type_id INT,
  CONSTRAINT fk_users_user_types
    FOREIGN KEY (user_type_id)
    REFERENCES user_types (id)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION
);

-- -----------------------------------------------------
-- Table projetoava.courses
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS courses (
  id SERIAL PRIMARY KEY,
  title VARCHAR(100) NOT NULL,
  description VARCHAR(500)
);

-- -----------------------------------------------------
-- Table projetoava.lessons
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS lessons (
  id SERIAL PRIMARY KEY,
  course_id INT NOT NULL,
  title VARCHAR(100) NOT NULL,
  description VARCHAR(500),
  embed_url VARCHAR(1000) NOT NULL,
  CONSTRAINT fk_lessons_courses
    FOREIGN KEY (course_id)
    REFERENCES courses (id)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION
);

-- -----------------------------------------------------
-- Table projetoava.user_lesson_visualizations
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS user_lesson_visualizations (
  id SERIAL PRIMARY KEY,
  user_id INT NOT NULL,
  lesson_id INT NOT NULL,
  visualized_at TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  CONSTRAINT fk_user_lesson_visualization_users
    FOREIGN KEY (user_id)
    REFERENCES users (id)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT fk_user_lesson_visualization_lessons
    FOREIGN KEY (lesson_id)
    REFERENCES lessons (id)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION
);