# Bittar Neurociência

Se você chegou até aqui é porque quer fazer parte da equipe de desenvolvimento da [Bittar Neurociência](https://bittarneurociencia.com.br/).

Caso ainda não tenha se cadastrado em nosso banco de talentos dirija-se para o nosso [Talent](https://talent.bittarneurociencia.com.br/talent/curriculo), cadastre seu currículo e faça o teste de perfilamento.

Caso já tenha se cadastrado, faça o nosso desafio técnico a seguir.

# Projeto AVA

Este projeto consiste no desenvolvimento de um Ambiente Virtual de Aprendizagem (AVA) simples utilizando React para o Frontend e NodeJs para o Backend.

Com o projeto finalizado conseguiremos avaliar sua capacidade em estruturar um projeto simples no formato Full Stack.

## O que será avaliado?

### Frontend
- Capacidade de transpor uma tela no Figma para o React;
- Componentização de objetos;
- Organização e legibilidade do código.

### Backend
- Organização e legibilidade do código;
- Lógica de programação;
- Tratamento de erros;
- Funcionalidades gerais de acordo com os requisitos.

## Requisitos do sistema

1. A plataforma deverá ter dois níveis de acesso, administrador e aluno;
2. O administrador deve ser capaz de listar/criar/editar/apagar alunos, cursos e aulas;
3. O aluno deve ser capaz de visualizar todos os cursos cadastrados e assistir todas as aulas disponíveis;
4. O aluno **não** deve conseguir criar/editar/apagar quaisquer informações na plataforma;
5. As aulas cadastradas deverão ser incorporações de vídeos do Youtube;
6. O menu de dashboard deverá ser visualizado apenas por um usuário administrador;
7. O menu de dashboard deverá conter os seguintes gráficos:
    - Ranking das aulas mais assistidas;
    - Porcentagem de visualização de todas as aulas em relação à quantidade total de visualizações;
    - Ranking dos alunos que mais assistiram aulas;
    - Porcentagem de visualização dos alunos em relação à quantidade total de visualizações;

A sua interpretação dos requisitos e da estrutura do banco de dados também será avaliada.
Fique à vontade para adicionar mais funcionalidades desde que não haja alteração na estrutura do banco de dados e que você tenha cumprido a lista de requisitos.

## Detalhes técnicos e estruturais

1. Faça o clone desse repositório para seu computador;
2. Separe seu código de Frontend e Backend nas respectivas pastas deste mesmo repositório;
3. O banco de dados a ser utilizado é o PostgreSQL na versão 13 ou superior. Utilize o script [database.sql](database.sql) para criar seu banco de dados local já com dados populados;
4. **Não** altere a estrutura do banco de dados pois utilizaremos o script [database.sql](database.sql) para testar sua aplicação;
5. Utilize os arquivos `README.md` dentro das pastas `frontend` e `backend` para detalhar como seu código deve ser compilado e executado;
6. Crie variáveis de ambiente para facilitar a configuração da sua aplicação e detalhe nos arquivos `README.md`.

![React](https://img.shields.io/badge/react-%2320232a.svg?style=for-the-badge&logo=react&logoColor=%2361DAFB)

![NodeJS](https://img.shields.io/badge/node.js-6DA55F?style=for-the-badge&logo=node.js&logoColor=white)

![Postgres](https://img.shields.io/badge/postgres-%23316192.svg?style=for-the-badge&logo=postgresql&logoColor=white)

### Figma

### Diagrama Entidade Relacionamento (DER)

![DER](der.png)

## Entrega

- Você deverá submeter um [Merge Request](https://docs.gitlab.com/ee/user/project/merge_requests/) para este mesmo repositório com todo o conteúdo adicional que você criou dentro das pastas [frontend](frontend) e [backend](backend);
- Dentro do Merge Request coloque seu nome completo na caixa de título;
- Certifique-se de que o código esteja funcional para que ele seja executado em um ambiente isolado;
- Documente seu código para uma compreensão facilitada;
- Boa sorte!